#include <iostream>
#include <string>

// the definition of a simple function that adds 2 numbers and returns the result.
// it also prints that it is doing the adding
int add(int num1, int num2){
    int temp = num1 + num2;
    std::cout << "I am in the add function" <<std::endl;
    return temp;
}
//a simple function that prints a given string and returns nothing
// notice that it needs no return statement.
void printString(std::string toBePrinted){
    std::cout << "I have been asked to print: " << toBePrinted << std::endl;
}

int main() {
    //printing the string "Hello, World!" to the console
    std::cout << "Hello, World!" << std::endl;

    int length = 9;
    //creating an array with length equal to the variable length
    int intArray[length];

    // this is a for loop see the slides for more information
    for (int i = 0; i < length; ++i) {
        intArray[i] = i * 3;
    }

    int count = 0;
    //  this is a while loop see the slides for more information
    while(count < length){
        //this is a more in-depth use of a print statement.
        std::cout << "Array at location " << count << " is equal to " << intArray[count] << std::endl;
        count++;
    }

    bool boolean = true;
    // this is an example of an if statement see the slides for more details
    if(boolean){
        std::cout << "boolean was true" << std::endl;
    }else{
        std::cout << "boolean was false" << std::endl;
    }

    // this is an example of setting an integer equal to the output of a function
    int added = add(intArray[0], 5);
    std::cout << "Added function is done, it returned " << added << std::endl;

    // this is an example of calling a function with no return value
    printString("I want to print this string");


    return 0;
}

