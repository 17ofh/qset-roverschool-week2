cmake_minimum_required(VERSION 3.5)
project(w2code)

set(CMAKE_CXX_STANDARD 11)

add_executable(w2code main.cpp)
